#include <iostream>
#include <algorithm>
#include <vector>
#include <tuple>
#include <limits>
#include <cmath>
#include <chrono>
using namespace std;
using namespace std::chrono;

tuple<int,int,int> find_maximum_subarray_exhaustive(vector<int>&A, int low,int high) {
    int max_sum = numeric_limits<int>::min();
    int left = -1;
    int right = -1;
    int sum = 0;
    for(int i = 0; i <= high; ++i) {
        sum = 0;  
        for (int j = i; j <= high;++j) {
            sum = sum + A[j];
            if (sum > max_sum) {
                max_sum = sum;
                left = i;
                right = j;
            }
        }
    }
    return {left,right,max_sum};
}

tuple<int,int,int> find_max_crossing_subarray(vector<int>& A, int low, int mid, int high) {
    int left_sum = numeric_limits<int>::min();
    int sum = 0;
    int max_left = -1;
    
    for (int i = mid; i >= low; --i) {
        sum = sum + A[i];
        if (sum > left_sum) {
            left_sum = sum;
            max_left = i;
        }
    }

    int right_sum = numeric_limits<int>::min();
    sum = 0;
    int max_right = -1;

    for (int j = mid+1; j <= high; ++j) {
        sum  = sum + A[j];
        if (sum > right_sum) {
            right_sum = sum;
            max_right = j;
        }
    }
    return {max_left, max_right, left_sum + right_sum};
}

tuple<int,int,int> find_maximum_subarray(vector<int>&A,int low, int high) {
    if (high == low) {
        return {low, high, A[low]};
    }
    int mid = static_cast<int>(floor((low+high)/2.0));
    const auto [left_low, left_high, left_sum] = find_maximum_subarray(A, low, mid);
    const auto [right_low,right_high,right_sum] = find_maximum_subarray(A,mid+1,high);
    const auto [cross_low, cross_high, cross_sum] = find_max_crossing_subarray(A,low,mid,high);

    if(left_sum >= right_sum && left_sum >= cross_sum) {
        return {left_low, left_high,left_sum};
    } else if (right_sum >= left_sum && right_sum >= cross_sum) {
        return {right_low,right_high,right_sum};
    } else return {cross_low,cross_high,cross_sum};

}

tuple<int,int,int> find_maximum_subarray2(vector<int>&A,int low, int high) {
    if (high == low) {
        return {low, high, A[low]};
    }
    // if (high - low < 95) {
    //     return find_maximum_subarray_exhaustive(A, low, high);
    // }
    int mid = static_cast<int>(floor((low+high)/2.0));
    const auto [left_low, left_high, left_sum] = find_maximum_subarray2(A, low, mid);
    const auto [right_low,right_high,right_sum] = find_maximum_subarray2(A,mid+1,high);
    const auto [cross_low, cross_high, cross_sum] = find_max_crossing_subarray(A,low,mid,high);

    if(left_sum >= right_sum && left_sum >= cross_sum) {
        return {left_low, left_high,left_sum};
    } else if (right_sum >= left_sum && right_sum >= cross_sum) {
        return {right_low,right_high,right_sum};
    } else return {cross_low,cross_high,cross_sum};

}



int main() {
    vector<int> arr;
    int u;
    while(cin >> u) {
        arr.push_back(u);
    }

    vector<int> values(10000);
 
    // Generate Random values
    auto f = []() -> int { return rand() % 10000 - 5000; };
 
    // Fill up the vector
    generate(values.begin(), values.end(), f);
 

    auto start = high_resolution_clock::now();
    
    auto [low, high, sum] = find_maximum_subarray(values, 0, (int)values.size()-1);
    
    auto stop = high_resolution_clock::now();

    cout << low << " " << high << " " << sum << endl;

    auto duration = duration_cast<microseconds>(stop - start);
 
    cout << "Time taken by function: "
         << duration.count() << " microseconds" << endl;

     start = high_resolution_clock::now();
    
    auto [low2, high2, sum2] = find_maximum_subarray2(values, 0, (int)values.size()-1);
    
    stop = high_resolution_clock::now();

    cout << low2 << " " << high2 << " " << sum2 << endl;

    duration = duration_cast<microseconds>(stop - start);
 
    cout << "Time taken by function: "
         << duration.count() << " microseconds" << endl;

    return 0;
}